import request from 'request-promise-native';
import cheerio from 'cheerio';
import { RESTErrorNotFound, RESTErrorBadRequest } from '../helpers/errors';

const appendResponse = (response, property, value, defaultMediaObject) => {
  if (response[response.length - 1]) {
    response[response.length - 1][property] = value;
  } else {
    const mediaObject = defaultMediaObject;
    mediaObject[property] = value;
    response.push(mediaObject);
  }
  return response;
};

const checkUrlInresponse = (response, url) => {
  for (let i = 0; i < response.length; i += 1) {
    if (response[i].url === url) {
      return false;
    }
  }

  return true;
};

const getBaseURL = fetchedURL => {
  let baseURL;
  let URLparts = fetchedURL.split('/');
  if (fetchedURL.startsWith('http')) {
    URLparts = URLparts.splice(0, 3);
    baseURL = URLparts.join('/');
    return baseURL;
  }
  return URLparts[0];
};

const formatImaggeURL = (imgURL, fetchedURL) => {
  if (!imgURL.match(/^\w+:\/\//) && !imgURL.startsWith('data:')) {
    if (imgURL.startsWith('//')) {
      return `http:${imgURL.substr(1)}`;
    }

    if (imgURL.startsWith('/')) {
      return getBaseURL(fetchedURL) + imgURL;
    }
    return `${getBaseURL(fetchedURL)}/${imgURL}`;
  }
  return imgURL;
};

const checkForRedirect = cheerioHTML => {
  let redirect;
  redirect = cheerioHTML("meta[http-equiv='REFRESH']").attr('content');
  if (!redirect || redirect === '') {
    redirect = cheerioHTML("meta[http-equiv='refresh']").attr('content');
  }

  if (redirect && redirect !== '') {
    const splittedContent = redirect.split(';');

    if (Number(splittedContent[0]) === 0) {
      redirect = splittedContent[1].slice(
        splittedContent[1].indexOf('=') + 1,
        splittedContent[1].length
      );
      if (redirect && redirect !== '') {
        return redirect;
      }
    }
  }
  return false;
};

export const loadHTML = async url => {
  let html;
  let finalUrl = url;

  if (!url.startsWith('http://') && !url.startsWith('https://')) {
    finalUrl = `http://${url}`;
  } else {
    finalUrl = url;
  }

  try {
    html = await request.get(finalUrl).on('response', response => {
      finalUrl = response.request.uri.href;
    });
  } catch (e) {
    if (e.statusCode === 404) {
      throw new RESTErrorNotFound();
    } else {
      throw new RESTErrorBadRequest('BAD_REQUEST', 'BAD_REQUEST', e);
    }
  }
  const $ = await cheerio.load(html);
  const redirect = checkForRedirect($);
  if (redirect) {
    const r = await loadHTML(redirect);
    return r;
  } else if (redirect === false) {
    const values = {
      cheerioHTML: $,
      url: finalUrl
    };
    return values;
  }
  return null;
};

export const getTitle = cheerioHTML => {
  let title;
  title = cheerioHTML("meta[property='og:title']").attr('content');
  if (title && title !== '') {
    return title.trim();
  }

  title = cheerioHTML('title').text();
  if (title && title !== '') {
    return title.trim();
  }

  title = cheerioHTML('h1')
    .first()
    .text();
  if (title && title !== '') {
    return title.trim();
  }

  return null;
};

export const getDescription = cheerioHTML => {
  let description;
  description = cheerioHTML("meta[property='og:description']").attr('content');
  if (description && description !== '') {
    return description.trim();
  }

  description = cheerioHTML("meta[name='description']").attr('content');
  if (description && description !== '') {
    return description.trim();
  }

  description = cheerioHTML('p')
    .first()
    .text();
  if (description && description !== '') {
    return description.trim();
  }

  return null;
};

const getMediaFromOgTag = (
  cheerioHTML,
  mediaType,
  defaultMediaObject,
  fetchedURL
) => {
  const meta = cheerioHTML('meta');
  const identificator = `og:${mediaType}`;
  let response = [];

  for (let i = 0; i < meta.length; i += 1) {
    const attributes = meta[i].attribs;
    const newMediaObject = Object.assign({}, defaultMediaObject);
    if (attributes.property && attributes.property.startsWith(identificator)) {
      const splittedProperty = attributes.property.split(':');
      if (splittedProperty.length === 2) {
        response.push(newMediaObject);
        response[response.length - 1].url = formatImaggeURL(
          attributes.content,
          fetchedURL
        );
      }
      if (splittedProperty.length === 3) {
        switch (splittedProperty[2]) {
          case 'secure_url':
            response = appendResponse(
              response,
              'secureUrl',
              attributes.content,
              newMediaObject
            );
            break;
          case 'url':
            response = appendResponse(
              response,
              'url',
              attributes.content,
              newMediaObject
            );
            break;
          case 'width':
            response = appendResponse(
              response,
              'width',
              attributes.content,
              newMediaObject
            );
            break;
          case 'height':
            response = appendResponse(
              response,
              'height',
              attributes.content,
              newMediaObject
            );
            break;
          case 'alt':
            if (mediaType === 'image') {
              response = appendResponse(
                response,
                'alt',
                attributes.content,
                newMediaObject
              );
            }
            break;
          default:
            break;
        }
      }
    }
  }
  return response;
};

const findAllImages = (cheerioHTML, defaultImageObject, fetchedUrl) => {
  const response = [];
  const images = cheerioHTML('img').toArray();
  for (let i = 0; i < images.length; i += 1) {
    const attributes = images[i].attribs;
    const image = Object.assign({}, defaultImageObject);
    if (attributes.src && attributes.src !== '') {
      if (checkUrlInresponse(response, attributes.src)) {
        image.url = formatImaggeURL(attributes.src, fetchedUrl);
        if (attributes.alt && attributes.alt !== '') {
          image.alt = attributes.alt;
        } else {
          image.alt = null;
        }
        response.push(image);
      }
    }
  }
  return response;
};

export const getImages = (cheerioHTML, fetchedUrl) => {
  const defaultImageObject = {
    url: null,
    secureUrl: null,
    width: null,
    height: null,
    alt: null
  };

  const ogImages = getMediaFromOgTag(
    cheerioHTML,
    'image',
    defaultImageObject,
    fetchedUrl
  );
  const allImages = findAllImages(cheerioHTML, defaultImageObject, fetchedUrl);
  const images = ogImages.concat(allImages);
  return images;
};

export const getVideos = (cheerioHTML, fetchedURL) => {
  const defaultVideoObject = {
    url: null,
    secureUrl: null,
    width: null,
    height: null
  };

  const videos = getMediaFromOgTag(
    cheerioHTML,
    'video',
    defaultVideoObject,
    fetchedURL
  );

  return videos;
};
