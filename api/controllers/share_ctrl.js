import {
  loadHTML,
  getTitle,
  getDescription,
  getImages,
  getVideos
} from '../services/share_service';

export const getDataForShare = async (req, res, next) => {
  try {
    const response = {};

    // načíta html aj pri redirecte
    const values = await loadHTML(req.swagger.params.url.value);

    const { cheerioHTML, url } = values;

    response.url = url;
    response.title = getTitle(cheerioHTML);
    response.description = getDescription(cheerioHTML);
    response.images = getImages(cheerioHTML, url);
    response.videos = getVideos(cheerioHTML, url);

    res.json(response);
  } catch (e) {
    next(e);
  }
};
