FROM alpine:3.7

# isntall  nodejs
RUN apk add --no-cache nodejs
# Create app directory
WORKDIR /usr/src/app

# copy package.json and package.lock in workdir
# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ./

# Install app dependencies
# If there is no change in package.json -> this layer will not be executed
RUN npm install --production


# Copy all files inside workdir.
COPY . .

# expose production port
EXPOSE 80

# run production script
CMD [ "npm", "run", "production" ]
